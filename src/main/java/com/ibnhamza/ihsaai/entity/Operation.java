/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ibnhamza.ihsaai.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Amine <amine.ihsm@ŋmail.com>
 */
@Entity
@Table(name = "operation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Operation.findAll", query = "SELECT o FROM Operation o")
    , @NamedQuery(name = "Operation.findById", query = "SELECT o FROM Operation o WHERE o.id = :id")
    , @NamedQuery(name = "Operation.findByDateOperation", query = "SELECT o FROM Operation o WHERE o.dateOperation = :dateOperation")
    , @NamedQuery(name = "Operation.findByPaye", query = "SELECT o FROM Operation o WHERE o.paye = :paye")
    , @NamedQuery(name = "Operation.findByReduction", query = "SELECT o FROM Operation o WHERE o.reduction = :reduction")
    , @NamedQuery(name = "Operation.findByResumeClinique", query = "SELECT o FROM Operation o WHERE o.resumeClinique = :resumeClinique")
    , @NamedQuery(name = "Operation.findByTotal", query = "SELECT o FROM Operation o WHERE o.total = :total")})
public class Operation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "date_operation")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOperation;
    @Lob
    @Column(name = "observation")
    private String observation;
    @Column(name = "paye")
    private Short paye;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "reduction")
    private Float reduction;
    @Column(name = "resume_clinique")
    private String resumeClinique;
    @Column(name = "total")
    private Float total;
    @OneToMany(mappedBy = "operationId")
    private Collection<DetailOperation> detailOperationCollection;
    @JoinColumn(name = "consulation_id", referencedColumnName = "id")
    @ManyToOne
    private Consultation consulationId;
    @JoinColumn(name = "fonctionnaire_id", referencedColumnName = "id")
    @ManyToOne
    private Fonctionnaire fonctionnaireId;
    @JoinColumn(name = "medecin_id", referencedColumnName = "id")
    @ManyToOne
    private Fonctionnaire medecinId;
    @JoinColumn(name = "patient_id", referencedColumnName = "id")
    @ManyToOne
    private Patient patientId;

    public Operation() {
    }

    public Operation(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateOperation() {
        return dateOperation;
    }

    public void setDateOperation(Date dateOperation) {
        this.dateOperation = dateOperation;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Short getPaye() {
        return paye;
    }

    public void setPaye(Short paye) {
        this.paye = paye;
    }

    public Float getReduction() {
        return reduction;
    }

    public void setReduction(Float reduction) {
        this.reduction = reduction;
    }

    public String getResumeClinique() {
        return resumeClinique;
    }

    public void setResumeClinique(String resumeClinique) {
        this.resumeClinique = resumeClinique;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    @XmlTransient
    public Collection<DetailOperation> getDetailOperationCollection() {
        return detailOperationCollection;
    }

    public void setDetailOperationCollection(Collection<DetailOperation> detailOperationCollection) {
        this.detailOperationCollection = detailOperationCollection;
    }

    public Consultation getConsulationId() {
        return consulationId;
    }

    public void setConsulationId(Consultation consulationId) {
        this.consulationId = consulationId;
    }

    public Fonctionnaire getFonctionnaireId() {
        return fonctionnaireId;
    }

    public void setFonctionnaireId(Fonctionnaire fonctionnaireId) {
        this.fonctionnaireId = fonctionnaireId;
    }

    public Fonctionnaire getMedecinId() {
        return medecinId;
    }

    public void setMedecinId(Fonctionnaire medecinId) {
        this.medecinId = medecinId;
    }

    public Patient getPatientId() {
        return patientId;
    }

    public void setPatientId(Patient patientId) {
        this.patientId = patientId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Operation)) {
            return false;
        }
        Operation other = (Operation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ibnhamza.ihsaai.entity.Operation[ id=" + id + " ]";
    }
    
}
