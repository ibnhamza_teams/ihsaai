/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ibnhamza.ihsaai.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Amine <amine.ihsm@ŋmail.com>
 */
@Entity
@Table(name = "consultation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Consultation.findAll", query = "SELECT c FROM Consultation c")
    , @NamedQuery(name = "Consultation.findById", query = "SELECT c FROM Consultation c WHERE c.id = :id")
    , @NamedQuery(name = "Consultation.findByDateConsultation", query = "SELECT c FROM Consultation c WHERE c.dateConsultation = :dateConsultation")
    , @NamedQuery(name = "Consultation.findByMotif", query = "SELECT c FROM Consultation c WHERE c.motif = :motif")})
public class Consultation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Lob
    @Column(name = "conduiteatenir")
    private String conduiteatenir;
    @Column(name = "dateConsultation")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateConsultation;
    @Lob
    @Column(name = "diagRetenu")
    private String diagRetenu;
    @Lob
    @Column(name = "diagSuppose")
    private String diagSuppose;
    @Lob
    @Column(name = "examenClinique")
    private String examenClinique;
    @Lob
    @Column(name = "examenDemande")
    private String examenDemande;
    @Lob
    @Column(name = "interrogatoire")
    private String interrogatoire;
    @Column(name = "motif")
    private String motif;
    @Lob
    @Column(name = "resultatExamen")
    private String resultatExamen;
    @JoinColumn(name = "patient", referencedColumnName = "id")
    @ManyToOne
    private Patient patient;
    @OneToMany(mappedBy = "consulationId")
    private Collection<Operation> operationCollection;

    public Consultation() {
    }

    public Consultation(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getConduiteatenir() {
        return conduiteatenir;
    }

    public void setConduiteatenir(String conduiteatenir) {
        this.conduiteatenir = conduiteatenir;
    }

    public Date getDateConsultation() {
        return dateConsultation;
    }

    public void setDateConsultation(Date dateConsultation) {
        this.dateConsultation = dateConsultation;
    }

    public String getDiagRetenu() {
        return diagRetenu;
    }

    public void setDiagRetenu(String diagRetenu) {
        this.diagRetenu = diagRetenu;
    }

    public String getDiagSuppose() {
        return diagSuppose;
    }

    public void setDiagSuppose(String diagSuppose) {
        this.diagSuppose = diagSuppose;
    }

    public String getExamenClinique() {
        return examenClinique;
    }

    public void setExamenClinique(String examenClinique) {
        this.examenClinique = examenClinique;
    }

    public String getExamenDemande() {
        return examenDemande;
    }

    public void setExamenDemande(String examenDemande) {
        this.examenDemande = examenDemande;
    }

    public String getInterrogatoire() {
        return interrogatoire;
    }

    public void setInterrogatoire(String interrogatoire) {
        this.interrogatoire = interrogatoire;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public String getResultatExamen() {
        return resultatExamen;
    }

    public void setResultatExamen(String resultatExamen) {
        this.resultatExamen = resultatExamen;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @XmlTransient
    public Collection<Operation> getOperationCollection() {
        return operationCollection;
    }

    public void setOperationCollection(Collection<Operation> operationCollection) {
        this.operationCollection = operationCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Consultation)) {
            return false;
        }
        Consultation other = (Consultation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ibnhamza.ihsaai.entity.Consultation[ id=" + id + " ]";
    }
    
}
