/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ibnhamza.ihsaai.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Amine <amine.ihsm@ŋmail.com>
 */
@Entity
@Table(name = "fonctionnaire")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fonctionnaire.findAll", query = "SELECT f FROM Fonctionnaire f")
    , @NamedQuery(name = "Fonctionnaire.findById", query = "SELECT f FROM Fonctionnaire f WHERE f.id = :id")
    , @NamedQuery(name = "Fonctionnaire.findByCompte", query = "SELECT f FROM Fonctionnaire f WHERE f.compte = :compte")
    , @NamedQuery(name = "Fonctionnaire.findByMotDePasse", query = "SELECT f FROM Fonctionnaire f WHERE f.motDePasse = :motDePasse")
    , @NamedQuery(name = "Fonctionnaire.findByAdresse", query = "SELECT f FROM Fonctionnaire f WHERE f.adresse = :adresse")
    , @NamedQuery(name = "Fonctionnaire.findByNom", query = "SELECT f FROM Fonctionnaire f WHERE f.nom = :nom")
    , @NamedQuery(name = "Fonctionnaire.findBySpecialite", query = "SELECT f FROM Fonctionnaire f WHERE f.specialite = :specialite")
    , @NamedQuery(name = "Fonctionnaire.findByTelephone", query = "SELECT f FROM Fonctionnaire f WHERE f.telephone = :telephone")
    , @NamedQuery(name = "Fonctionnaire.findByRoleId", query = "SELECT f FROM Fonctionnaire f WHERE f.roleId = :roleId")
    , @NamedQuery(name = "Fonctionnaire.findByAffectation", query = "SELECT f FROM Fonctionnaire f WHERE f.affectation = :affectation")
    , @NamedQuery(name = "Fonctionnaire.findByChr", query = "SELECT f FROM Fonctionnaire f WHERE f.chr = :chr")
    , @NamedQuery(name = "Fonctionnaire.findByClassification", query = "SELECT f FROM Fonctionnaire f WHERE f.classification = :classification")
    , @NamedQuery(name = "Fonctionnaire.findByConges", query = "SELECT f FROM Fonctionnaire f WHERE f.conges = :conges")
    , @NamedQuery(name = "Fonctionnaire.findByDateEntree", query = "SELECT f FROM Fonctionnaire f WHERE f.dateEntree = :dateEntree")
    , @NamedQuery(name = "Fonctionnaire.findByHoraire", query = "SELECT f FROM Fonctionnaire f WHERE f.horaire = :horaire")
    , @NamedQuery(name = "Fonctionnaire.findByIActe", query = "SELECT f FROM Fonctionnaire f WHERE f.iActe = :iActe")
    , @NamedQuery(name = "Fonctionnaire.findByIrg", query = "SELECT f FROM Fonctionnaire f WHERE f.irg = :irg")
    , @NamedQuery(name = "Fonctionnaire.findByModePaiement", query = "SELECT f FROM Fonctionnaire f WHERE f.modePaiement = :modePaiement")
    , @NamedQuery(name = "Fonctionnaire.findByModeRecrutement", query = "SELECT f FROM Fonctionnaire f WHERE f.modeRecrutement = :modeRecrutement")
    , @NamedQuery(name = "Fonctionnaire.findByNCompte", query = "SELECT f FROM Fonctionnaire f WHERE f.nCompte = :nCompte")
    , @NamedQuery(name = "Fonctionnaire.findByNSs", query = "SELECT f FROM Fonctionnaire f WHERE f.nSs = :nSs")
    , @NamedQuery(name = "Fonctionnaire.findByNiveauUniversitaire", query = "SELECT f FROM Fonctionnaire f WHERE f.niveauUniversitaire = :niveauUniversitaire")
    , @NamedQuery(name = "Fonctionnaire.findByPhoto", query = "SELECT f FROM Fonctionnaire f WHERE f.photo = :photo")
    , @NamedQuery(name = "Fonctionnaire.findByPrenom", query = "SELECT f FROM Fonctionnaire f WHERE f.prenom = :prenom")
    , @NamedQuery(name = "Fonctionnaire.findByRss", query = "SELECT f FROM Fonctionnaire f WHERE f.rss = :rss")
    , @NamedQuery(name = "Fonctionnaire.findBySalaire", query = "SELECT f FROM Fonctionnaire f WHERE f.salaire = :salaire")
    , @NamedQuery(name = "Fonctionnaire.findBySituationFamiliale", query = "SELECT f FROM Fonctionnaire f WHERE f.situationFamiliale = :situationFamiliale")
    , @NamedQuery(name = "Fonctionnaire.findBySup", query = "SELECT f FROM Fonctionnaire f WHERE f.sup = :sup")
    , @NamedQuery(name = "Fonctionnaire.findByTitre", query = "SELECT f FROM Fonctionnaire f WHERE f.titre = :titre")
    , @NamedQuery(name = "Fonctionnaire.findBySexe", query = "SELECT f FROM Fonctionnaire f WHERE f.sexe = :sexe")
    , @NamedQuery(name = "Fonctionnaire.findByDateNaissance", query = "SELECT f FROM Fonctionnaire f WHERE f.dateNaissance = :dateNaissance")})
public class Fonctionnaire implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "compte")
    private String compte;
    @Column(name = "mot_de_passe")
    private String motDePasse;
    @Column(name = "adresse")
    private String adresse;
    @Column(name = "nom")
    private String nom;
    @Column(name = "specialite")
    private String specialite;
    @Column(name = "telephone")
    private String telephone;
    @Column(name = "role_id")
    private Integer roleId;
    @Column(name = "affectation")
    private String affectation;
    @Column(name = "chr")
    private Integer chr;
    @Column(name = "classification")
    private String classification;
    @Column(name = "conges")
    private Integer conges;
    @Column(name = "date_entree")
    @Temporal(TemporalType.DATE)
    private Date dateEntree;
    @Column(name = "horaire")
    private String horaire;
    @Column(name = "i_acte")
    private Integer iActe;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "irg")
    private Float irg;
    @Column(name = "mode_paiement")
    private String modePaiement;
    @Column(name = "mode_recrutement")
    private String modeRecrutement;
    @Column(name = "n_compte")
    private String nCompte;
    @Column(name = "n_ss")
    private String nSs;
    @Column(name = "niveau_universitaire")
    private String niveauUniversitaire;
    @Lob
    @Column(name = "observation")
    private String observation;
    @Column(name = "photo")
    private String photo;
    @Column(name = "prenom")
    private String prenom;
    @Column(name = "rss")
    private Float rss;
    @Column(name = "salaire")
    private Float salaire;
    @Column(name = "situation_familiale")
    private String situationFamiliale;
    @Column(name = "sup")
    private Integer sup;
    @Column(name = "titre")
    private String titre;
    @Column(name = "sexe")
    private String sexe;
    @Column(name = "date_naissance")
    @Temporal(TemporalType.DATE)
    private Date dateNaissance;
    @OneToMany(mappedBy = "medecinId")
    private Collection<DetailOperation> detailOperationCollection;
    @OneToMany(mappedBy = "fonctionnaireId")
    private Collection<Operation> operationCollection;
    @OneToMany(mappedBy = "medecinId")
    private Collection<Operation> operationCollection1;

    public Fonctionnaire() {
    }

    public Fonctionnaire(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompte() {
        return compte;
    }

    public void setCompte(String compte) {
        this.compte = compte;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getSpecialite() {
        return specialite;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getAffectation() {
        return affectation;
    }

    public void setAffectation(String affectation) {
        this.affectation = affectation;
    }

    public Integer getChr() {
        return chr;
    }

    public void setChr(Integer chr) {
        this.chr = chr;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public Integer getConges() {
        return conges;
    }

    public void setConges(Integer conges) {
        this.conges = conges;
    }

    public Date getDateEntree() {
        return dateEntree;
    }

    public void setDateEntree(Date dateEntree) {
        this.dateEntree = dateEntree;
    }

    public String getHoraire() {
        return horaire;
    }

    public void setHoraire(String horaire) {
        this.horaire = horaire;
    }

    public Integer getIActe() {
        return iActe;
    }

    public void setIActe(Integer iActe) {
        this.iActe = iActe;
    }

    public Float getIrg() {
        return irg;
    }

    public void setIrg(Float irg) {
        this.irg = irg;
    }

    public String getModePaiement() {
        return modePaiement;
    }

    public void setModePaiement(String modePaiement) {
        this.modePaiement = modePaiement;
    }

    public String getModeRecrutement() {
        return modeRecrutement;
    }

    public void setModeRecrutement(String modeRecrutement) {
        this.modeRecrutement = modeRecrutement;
    }

    public String getNCompte() {
        return nCompte;
    }

    public void setNCompte(String nCompte) {
        this.nCompte = nCompte;
    }

    public String getNSs() {
        return nSs;
    }

    public void setNSs(String nSs) {
        this.nSs = nSs;
    }

    public String getNiveauUniversitaire() {
        return niveauUniversitaire;
    }

    public void setNiveauUniversitaire(String niveauUniversitaire) {
        this.niveauUniversitaire = niveauUniversitaire;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Float getRss() {
        return rss;
    }

    public void setRss(Float rss) {
        this.rss = rss;
    }

    public Float getSalaire() {
        return salaire;
    }

    public void setSalaire(Float salaire) {
        this.salaire = salaire;
    }

    public String getSituationFamiliale() {
        return situationFamiliale;
    }

    public void setSituationFamiliale(String situationFamiliale) {
        this.situationFamiliale = situationFamiliale;
    }

    public Integer getSup() {
        return sup;
    }

    public void setSup(Integer sup) {
        this.sup = sup;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    @XmlTransient
    public Collection<DetailOperation> getDetailOperationCollection() {
        return detailOperationCollection;
    }

    public void setDetailOperationCollection(Collection<DetailOperation> detailOperationCollection) {
        this.detailOperationCollection = detailOperationCollection;
    }

    @XmlTransient
    public Collection<Operation> getOperationCollection() {
        return operationCollection;
    }

    public void setOperationCollection(Collection<Operation> operationCollection) {
        this.operationCollection = operationCollection;
    }

    @XmlTransient
    public Collection<Operation> getOperationCollection1() {
        return operationCollection1;
    }

    public void setOperationCollection1(Collection<Operation> operationCollection1) {
        this.operationCollection1 = operationCollection1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fonctionnaire)) {
            return false;
        }
        Fonctionnaire other = (Fonctionnaire) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ibnhamza.ihsaai.entity.Fonctionnaire[ id=" + id + " ]";
    }
    
}
