/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ibnhamza.ihsaai.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Amine <amine.ihsm@ŋmail.com>
 */
@Entity
@Table(name = "detail_operation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailOperation.findAll", query = "SELECT d FROM DetailOperation d")
    , @NamedQuery(name = "DetailOperation.findById", query = "SELECT d FROM DetailOperation d WHERE d.id = :id")})
public class DetailOperation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "acte_id", referencedColumnName = "id")
    @ManyToOne
    private Acte acteId;
    @JoinColumn(name = "operation_id", referencedColumnName = "id")
    @ManyToOne
    private Operation operationId;
    @JoinColumn(name = "medecin_id", referencedColumnName = "id")
    @ManyToOne
    private Fonctionnaire medecinId;

    public DetailOperation() {
    }

    public DetailOperation(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Acte getActeId() {
        return acteId;
    }

    public void setActeId(Acte acteId) {
        this.acteId = acteId;
    }

    public Operation getOperationId() {
        return operationId;
    }

    public void setOperationId(Operation operationId) {
        this.operationId = operationId;
    }

    public Fonctionnaire getMedecinId() {
        return medecinId;
    }

    public void setMedecinId(Fonctionnaire medecinId) {
        this.medecinId = medecinId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailOperation)) {
            return false;
        }
        DetailOperation other = (DetailOperation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ibnhamza.ihsaai.entity.DetailOperation[ id=" + id + " ]";
    }
    
}
