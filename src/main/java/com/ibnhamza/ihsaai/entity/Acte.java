/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ibnhamza.ihsaai.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Amine <amine.ihsm@ŋmail.com>
 */
@Entity
@Table(name = "acte")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Acte.findAll", query = "SELECT a FROM Acte a")
    , @NamedQuery(name = "Acte.findById", query = "SELECT a FROM Acte a WHERE a.id = :id")
    , @NamedQuery(name = "Acte.findByNomenclatureId", query = "SELECT a FROM Acte a WHERE a.nomenclatureId = :nomenclatureId")
    , @NamedQuery(name = "Acte.findByCotation", query = "SELECT a FROM Acte a WHERE a.cotation = :cotation")
    , @NamedQuery(name = "Acte.findByCout", query = "SELECT a FROM Acte a WHERE a.cout = :cout")
    , @NamedQuery(name = "Acte.findByTarif", query = "SELECT a FROM Acte a WHERE a.tarif = :tarif")
    , @NamedQuery(name = "Acte.findByTypeTube", query = "SELECT a FROM Acte a WHERE a.typeTube = :typeTube")
    , @NamedQuery(name = "Acte.findByNormes", query = "SELECT a FROM Acte a WHERE a.normes = :normes")
    , @NamedQuery(name = "Acte.findByIdReactif", query = "SELECT a FROM Acte a WHERE a.idReactif = :idReactif")
    , @NamedQuery(name = "Acte.findByDureeEstime", query = "SELECT a FROM Acte a WHERE a.dureeEstime = :dureeEstime")
    , @NamedQuery(name = "Acte.findByRaccourcis", query = "SELECT a FROM Acte a WHERE a.raccourcis = :raccourcis")})
public class Acte implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nomenclature_id")
    private int nomenclatureId;
    @Basic(optional = false)
    @Column(name = "cotation")
    private String cotation;
    @Lob
    @Column(name = "description")
    private String description;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cout")
    private Float cout;
    @Column(name = "tarif")
    private Float tarif;
    @Column(name = "type_tube")
    private Integer typeTube;
    @Column(name = "normes")
    private String normes;
    @Column(name = "id_reactif")
    private Integer idReactif;
    @Column(name = "duree_estime")
    private Integer dureeEstime;
    @Column(name = "raccourcis")
    private Boolean raccourcis;
    @OneToMany(mappedBy = "acteId")
    private Collection<DetailOperation> detailOperationCollection;

    public Acte() {
    }

    public Acte(Integer id) {
        this.id = id;
    }

    public Acte(Integer id, int nomenclatureId, String cotation) {
        this.id = id;
        this.nomenclatureId = nomenclatureId;
        this.cotation = cotation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getNomenclatureId() {
        return nomenclatureId;
    }

    public void setNomenclatureId(int nomenclatureId) {
        this.nomenclatureId = nomenclatureId;
    }

    public String getCotation() {
        return cotation;
    }

    public void setCotation(String cotation) {
        this.cotation = cotation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getCout() {
        return cout;
    }

    public void setCout(Float cout) {
        this.cout = cout;
    }

    public Float getTarif() {
        return tarif;
    }

    public void setTarif(Float tarif) {
        this.tarif = tarif;
    }

    public Integer getTypeTube() {
        return typeTube;
    }

    public void setTypeTube(Integer typeTube) {
        this.typeTube = typeTube;
    }

    public String getNormes() {
        return normes;
    }

    public void setNormes(String normes) {
        this.normes = normes;
    }

    public Integer getIdReactif() {
        return idReactif;
    }

    public void setIdReactif(Integer idReactif) {
        this.idReactif = idReactif;
    }

    public Integer getDureeEstime() {
        return dureeEstime;
    }

    public void setDureeEstime(Integer dureeEstime) {
        this.dureeEstime = dureeEstime;
    }

    public Boolean getRaccourcis() {
        return raccourcis;
    }

    public void setRaccourcis(Boolean raccourcis) {
        this.raccourcis = raccourcis;
    }

    @XmlTransient
    public Collection<DetailOperation> getDetailOperationCollection() {
        return detailOperationCollection;
    }

    public void setDetailOperationCollection(Collection<DetailOperation> detailOperationCollection) {
        this.detailOperationCollection = detailOperationCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acte)) {
            return false;
        }
        Acte other = (Acte) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ibnhamza.ihsaai.entity.Acte[ id=" + id + " ]";
    }
    
}
